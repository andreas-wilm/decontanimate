## This module is for decontaNIMation of FastQ reads.
## It requires BWA(-MEM) to be installed.


import osproc
import strutils
import hts/bam# to avoid ambigious Header identifier; see https://github.com/brentp/hts-nim/issues/25#issuecomment-418738924
import tables
import zip/gzipfiles
import logging
import os


const ContBamPrefix* = ".cont.bam"
const Fq1Suffix* = ".decont-R1.fastq.gz"
const Fq2Suffix* = ".decont-R2.fastq.gz"


type FastqRecord* = object
  ## A FastQ record, (with optional description in name).
  ## Borrowed from `jhbadger's bioseq <https://github.com/jhbadger/nimbioseq.git>`_
  id: string
  description: string
  qualities: string
  sequence: string

  
type NucAlphabet* = enum
  ## Simple enum for nucleid acids alphabets
  DNA, RNA

  
# From the docs "Warning: The global list of handlers is a thread var,
# this means that the handlers must be re-added in each thread."
var L = newConsoleLogger()
addHandler(L)


proc `$`*(rec: FastqRecord): string =
  ## Returns formatted string of FastQ record ``rec``.
  ## Borrowed from `jhbadger's bioseq <https://github.com/jhbadger/nimbioseq.git>`_
  var header = "@" & rec.id
  if rec.description != "":
    header = header & " " & rec.description
  header & "\n" & rec.sequence & "\n+\n" & rec.qualities
    
    
proc complement*(nucleicAcid: string, toAlphabet: NucAlphabet): string =
  ## Returns complement of a nucleic acid. Case aware. 
  ## IUPAC ambiguity characters will raise ValueError.
  result = newString(nucleicAcid.len)# FIXME would it be faster to copy input and change in place?
  let ignChars = ['-', 'N', 'n']
  var trans = {'C': 'G', 'c': 'g',
                'G': 'C', 'g': 'c',
                'U': 'A', 'u': 'a',
                'T': 'A', 't': 'a'
              }.toTable

  if toAlphabet == DNA:
      trans['A'] = 'T'
      trans['a'] = 't'
  elif toAlphabet == RNA:
      trans['A'] = 'U'
      trans['a'] = 'u'
  else:
    raise newException(ValueError, "Unknown Alphabet " & $toAlphabet)

  for i, nuc in nucleicAcid:
    if nuc in ignChars:
      result[i] = nuc
    else:
      doAssert trans.hasKey(nuc)
      result[i] = trans[nuc]

  
proc reversed(s: string): string =
  ## Return reversed copy of input string.
  ## From https://www.rosettacode.org/wiki/Reverse_a_string
  result = newString(s.len)
  for i, c in s:
    result[s.high - i] = c

    
proc phredToAscii*(phredQual: uint8): char =
  ## Encode ASCII encoded phred score
  # FIXME any advantage to using inline?
  result = chr(phredQual+33)


proc bamToFastqRecord*(bamRec: Record): FastqRecord =
  ## Convert BAM record to FastQ record
  result.id = bamRec.qname
  result.description = ""
  
  var quals = newSeq[uint8]()
  discard bamRec.base_qualities(quals)
  result.qualities = newString(len(quals))
  for i, x in quals:
    result.qualities[i] = phredToAscii(x)
    
  result.sequence = ""#newString(len(quals))# murky: "" lead to SIGSEGV after reentry
  discard bamRec.sequence(result.sequence)# as in htsnim tests

  if reverse(bamRec.flag):
    result.qualities = reversed(result.qualities)
    result.sequence = reversed(complement(result.sequence, DNA))

  assert len(result.qualities) == len(result.sequence)
  return result


iterator execBwaMem*(fq1: string, fq2: string, reffa: string,
                     bamHeader: var Header, threads=1,
                     isInterleaved=false): Record = # {.closure.} =
  ## Call BWA MEM with FastQ files ``fq1`` and ``fq2`` against
  ## reference ``reffa`` and return BAM records as iterator
  let cmd = "bwa"
  var args = @["mem", "-v", "1", "-t", $threads]
  var line = TaintedString("")
  var headerPassed = false
  var headerStr = ""
  
  if isInterleaved:
    args.add("-p")
  args.add(reffa)
  args.add(fq1)
  if fq2 != "":
    args.add(fq2)

  info("Running: " & cmd & " " & join(args, " "))
  var process = startProcess(cmd, args=args, options={poUsePath})
  while process.outputStream.readLine(line):
    if not headerPassed:
      if line.startswith('@'):
        headerStr = headerStr & line
        continue
      else:
        headerPassed = true
        bamHeader.fromString(headerStr)
    var rec = NewRecord(bamHeader)# FIXME is this a mem leak?
    rec.from_string(line)
    # secondary/supplementary mappings "duplicate" reads, so ignore
    if rec.flag.secondary == true or rec.flag.supplementary == true:
      continue
    yield rec
    
  while process.errorStream.readLine(line):
    if line.startswith("[E::"):
      stderr.writeLine(line)
    # FIXME here's a chance to interpret, e.g. [E::bwa_idx_load_from_disk] fail to locate the index files
      
  process.close
  var rc = process.peekExitCode()
  if rc != 0:
    raise newException(ValueError, "BWA exited with error code " & $rc)


proc readBaseName(qname: string): string =
  ## Returns basename of read name, i.e. without trailing indicator of
  ## read number in pair (if present)
  if len(qname) < 3:
    result = qname    
  elif qname[^2] in "#/" and isDigit(qname[^1]):# FIXME isnum?
    result = qname[0..^3]
  else:
    result = qname


proc verifyPairedReadNames(qname1: string, qname2: string) =
  ## Verify read names in given pair match
  var b1 = readBaseName(qname1)
  var b2 = readBaseName(qname2)
  if b1 != b2:
    raise newException(
      ValueError, "read basename mismatch in pair: $1 != $2" % [b1, b2])


proc verifyReadPair(r1: Record, r2: Record) =
  ## Verify that BAM record pair is indeed a valid pair
  verifyPairedReadNames(r1.qname, r2.qname)
  if read1(r1.flag) == true:
    if read2(r2.flag) == false:
      raise newException(
        ValueError, "pairing info invalid for read pair $1 != $2" % [r1.qname, r2.qname])
  else:
    if read2(r2.flag) == true:
      raise newException(
        ValueError, "pairing info invalid for read pair $1 != $2" % [r1.qname, r2.qname])
        

proc alnCoverage(rec: Record): float =
  ## Compute alignment/reference coverage of an aligned read
  var cov: Natural
  var sq = ""
  for se in rec.cigar.ref_coverage(ipos=rec.start):
    assert se[1] > se[0]
    cov += se[1] - se[0]
  result = cov/len(rec.sequence(sq))


proc decontanimate*(fq1: string, fq2: string = "", reffa: string, outpref: string,
                    mincov=0.5, threads: Natural = 1, reportEvery: Natural = 1000000,
                    isInterleaved = false) =
  ## Decontaminates reads in given FastQ files ``fq1`` (and optional ``fq2``) by
  ## mapping against reference ``reffa`` using BWA with ``threads`` threads). Writes
  ## clean reads to FastQ and contaminate reads to BAM using
  ## ``outpref``. Read or pair is considered contaminated if one has at
  ## least ``mincov`` alignment coverage. If `isInterleaved` is true then it's assumed that
  ## `fq1` is interleaved (and `fq2` is invalid)
  var read1: Record
  var read2: Record
  var headerWritten = false
  var isPairedEnd: bool# also true for interleaved
  var bamHeader = Header()
  var bamWriter: Bam
  var fq1Stream: GzFileStream
  var fq2Stream: GzFileStream
  var numContPairs = 0# for SE 'pairs' actually means reads
  var numCleanPairs = 0# for SE 'pairs' actually means read
                         
  if not fileExists(reffa):
    fatal("Reference fasta file does not exist: " & reffa)
    quit(QuitFailure)
  if not fileExists(fq1):
    fatal("FastQ1 does not exist: " & fq1)
    quit(QuitFailure)
  if fq2 != "":
    if not fileExists(fq2):
      fatal("FastQ2 does not exist:" & fq2)
      quit(QuitFailure)

  if fq2 != "" or isInterleaved:
    isPairedEnd = true
  else:
    isPairedEnd = false
  
  if isInterleaved:
    if fq2 != "":
      fatal("In interleaved mode FastQ2 is not allowed")
      quit(QuitFailure)

  if isPairedEnd==true and fq1 == fq2:
    warn("FastQ filenames are identical. Are you sure this is what you want?")

  var units = if isPairedEnd==true: "pairs" else: "reads"

  open(bamWriter, outpref & ContBamPrefix, mode="wb")
  # FIXME check bamWriter for nil?
  
  fq1Stream = newGZFileStream(outpref & Fq1Suffix, fmWrite)
  if fq1Stream == nil:
    fatal("Unable to open FastQ output files with prefix ", outpref)
    quit(QuitFailure)
  if isPairedEnd:
    fq2Stream = newGZFileStream(outpref & Fq2Suffix, fmWrite)
    if fq2Stream == nil:
      fatal("Unable to open FastQ output files with prefix ", outpref)
      quit(QuitFailure)

  var readNo = 0# it offset read count. crutch for the lack of zip/pairs
  for r in execBwaMem(fq1, fq2, reffa, bamHeader, 
    threads=threads, isInterleaved=isInterleaved):
    inc readNo
    if readNo == 1:
      read1 = r        
    elif readNo == 2:
      read2 = r
      verifyReadPair(read1, read2)
      
    # process if SE or 2nd for PE parsed
    if (isPairedEnd==false and readNo==1) or (isPairedEnd==true and readNo==2):
      readNo = 0
      if alnCoverage(read1) >= mincov or (isPairedEnd==true and alnCoverage(read2) >= mincov):
        # contaminants to BAM
        if not headerWritten:
          bamWriter.write_header(bamHeader)
          headerWritten = true
        bamWriter.write(read1)
        if isPairedEnd:
          bamWriter.write(read2)
        inc numContPairs
      else:
        # clean reads to fastq
        var fqRec1 = bamToFastqRecord(read1)
        write(fq1Stream, $fqRec1 & "\n")
        if isPairedEnd:
           var fqRec2 = bamToFastqRecord(read2)
           write(fq2Stream, $fqRec2 & "\n")
        inc numCleanPairs

      if reportEvery != 0 and (numContPairs + numCleanPairs) %% reportEvery == 0:
        info("Processed ", numContPairs + numCleanPairs, " ", units)

  bamWriter.close()
  fq1Stream.close()
  if isPairedEnd:
    fq2Stream.close()

  info(numCleanPairs, " clean ", units, " and ",
       numContPairs, " contaminated " , units, " found.")
    
  
when isMainModule:
  # FIXME add option to apply logic to pair instead of single read
  import cligen
  dispatch(decontanimate, help= {
    "fq1":  "FastQ1",
    "fq2" : "FastQ2 (optional)",
    "reffa": "BWA-indexed reference fasta to decontaminate against",
    "outpref": "Output prefix (" & ContBamPrefix & ", " & Fq1Suffix &
      " and " & Fq2Suffix & " will be added to respective output files)",
    "mincov": "Minimum coverage for aligned read to consider as contamination",
    "threads": "Number of BWA threads to use",
    "reportEvery": "Print status every x reads/pairs",
    "isInterleaved": "FastQ1 is interleaved"
    },
    short = {
      "fq1": '1',
      "fq2": '2',
      "reffa": 'r',
      "outpref": 'o',
      "mincov": 'c',
      "threads": 't',
      "reportEvery": 'n',
      "isInterleaved": 'l'
      }
    )
