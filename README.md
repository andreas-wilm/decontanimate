decontanimate
=============

Decontaminate FastQ files by mapping with BWA-MEM against a given
reference.

This is a Nim reimplementation of https://github.com/CSB5/decont/ (hence the name ;))

Performs a mapping of given (SE or PE) reads with BWA-MEM against given source
fasta of contamination and produces an (unsorted) BAM file with contaminated
reads (one mate mapping suffices to make pair count as contamination)
and clean fastq.gz file(s) with uncontaminated reads.


## Prerequisites

You need [BWA-MEM](https://github.com/lh3/bwa) installed and in your PATH. 

## Usage

This program decontaminates reads in given FastQ files ``fq1`` (and optional ``fq2``) by mapping against reference ``reffa`` using BWA with ``threads`` number of threads. It writes clean reads to FastQ and contaminate reads to BAM using ``outpref``. A read (or read pair) is considered contaminated if it (at least one read) has at least ``mincov`` alignment coverage.

Run `decontanimate -h` to generate usage:

    -h, --help                         print this help
    -1=, --fq1=      string  REQUIRED  FastQ1
    -2=, --fq2=      string  ""        FastQ2 (optional)
    -r=, --reffa=    string  REQUIRED  BWA-indexed reference fasta to decontaminate against
    -o=, --outpref=  string  REQUIRED  Output prefix (.cont.bam, .decont-R1.fastq.gz and .decont-R2.fastq.gz will be added to respective output files)
    -c=, --mincov=   float   0.5       Minimum coverage for aligned read to consider as contamination
    -t=, --threads=  int     1         Number of BWA threads to use
    --reportEvery=   int     1000000   Print status every x reads/pairs


## Installation

### From binary release

Binaries are build with Brent Pedersen's awesome `hts_nim_static_builder`, part of [hts-nim](https://github.com/brentp/hts-nim)

Unfortunately Bitbucket doesn't support binary releases per se, but statically compiled binaries can be found in the releases folder.

After downloading the binary, you might have to make it executable with `chmod +x`

### From source

To compile decontanimate you need a [Nim](https://nim-lang.org/) installation.

To build just run `nim build`. This will install other Nim dependencies. See `decontanimate.nimble` for Nim library dependencies.

To run the binary, you will in addition need an [htslib installation](https://github.com/samtools/htslib) in your LD_LIBRARY_PATH.

Then run built-in tests with: `nim test`





