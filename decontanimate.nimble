version       = "0.0.1"
author        = "Andreas Wilm and others"
description   = "BWA based FastQ decontamination"
license       = "MIT"

requires "nim >= 0.18"
requires "cligen >= 0.9.11"
requires "hts >= 0.2.4"
requires "zip >= 0.2.1"

srcDir = "src"

bin = @["decontanimate"]

skipDirs = @["tests"]
skipExt = @["nim"]

task test, "run tests":
  withDir "tests":
    exec "nim c -d:debug -d:useGcAssert -d:useSysAssert -d:nimBurnFree --debugger:native -r all"

