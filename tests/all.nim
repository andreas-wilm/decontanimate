import unittest
import os
import zip/gzipfiles
import hts
import ../src/decontanimate


proc bamReadCount(f: string): Natural =
  var bamReader: Bam
  result = 0
  open(bamReader, f)
  for record in bamReader:
    inc result
  bamReader.close()


proc fastqReadCount(f: string): Natural =
  # FIXME for proper content check we should use a proper reader, e.g.
  # functions from
  # https://github.com/jhbadger/nimbioseq/blob/master/src/nimbioseq.nim
  # That makes sense if we export the other functions also provided
  # here as extra module
  var line = ""
  var s = newGZFileStream(f, fmRead)
  while s.readLine(line):
    inc result
  s.close()
  doAssert result%%4 == 0
  result = Natural(float(result)/4.0)# FIXME Eeek

  
suite "helpers":
  test "complement":
    check complement("ACGTNacgtnUu", DNA) == "TGCANtgcanAa"
    check complement("ACGTNacgtnUu", RNA) == "UGCANugcanAa"

  test "phredToAscii":
    check phredToAscii(2) == '#'


suite "All clean":
  let reffa = "data/denv1_refseq.fa"
  var fq1 = "data/SRR1056478_denv1_nomatch_1.fastq.gz"
  var fq2 = "data/SRR1056478_denv1_nomatch_2.fastq.gz"
  var fqi = "data/SRR1056478_denv1_nomatch_interleaved.fastq.gz"
  let outpref = "/tmp/decontanimate"# FIXME
  let fq1Out = outpref & Fq1Suffix
  let fq2Out = outpref & Fq2Suffix
  let bamOut = outpref & ContBamPrefix
  var readCount: Natural

  test "paired end":
    decontanimate(fq1, fq2, reffa, outpref, reportEvery=0)
    check(fileExists(fq1Out))
    check(fileExists(fq2Out))
    check(fileExists(bamOut))
    # BAM should be empty
    readCount = bamReadCount(bamOut)
    check(readCount == 0)
    # Both FastQ out should contain as many reads as fastq in  
    readCount = fastqReadCount(fq1)
    check(readCount == fastqReadCount(fq1Out))
    check(readCount == fastqReadCount(fq2Out))
    # clean up
    removeFile(fq1Out)
    removeFile(fq2Out)
    removeFile(bamOut)

  test "single end":
    # similar for SE
    decontanimate(fq1, "", reffa, outpref, reportEvery=0)
    check(fileExists(fq1Out))
    check(fileExists(bamOut))
    check(not fileExists(fq2Out))
    # BAM should be empty
    readCount = bamReadCount(bamOut)
    check(readCount == 0)
    # Fastq out should contain as many reads as fastq in  
    readCount = fastqReadCount(fq1)
    check(readCount == fastqReadCount(fq1Out))
    # clean up
    removeFile(fq1Out)
    removeFile(bamOut)
      
    test "interleaved":
      # similar for interleaved
      decontanimate(fqi, "", reffa, outpref, reportEvery=0, isInterleaved=true)
      check(fileExists(fq1Out))
      check(fileExists(bamOut))
      check(fileExists(fq2Out))
      # BAM should be empty
      readCount = bamReadCount(bamOut)
      check(readCount == 0)
      # Fastq out should contain as many reads as fastq in  
      readCount = fastqReadCount(fq1)
      check(readCount == fastqReadCount(fq1Out))
      # clean up
      removeFile(fq1Out)
      removeFile(bamOut)
    
suite "All cont":
  let reffa = "data/denv1_refseq.fa"
  var fq1 = "data/SRR1056478_denv1_match_1.fastq.gz"
  var fq2 = "data/SRR1056478_denv1_match_2.fastq.gz"
  var fqi = "data/SRR1056478_denv1_match_interleaved.fastq.gz"
  let outpref = "/tmp/decontanimate"# FIXME
  let fq1Out = outpref & Fq1Suffix
  let fq2Out = outpref & Fq2Suffix
  let bamOut = outpref & ContBamPrefix
  var readCount: Natural

  test "paired end":
    decontanimate(fq1, fq2, reffa, outpref, reportEvery=0)
    check(fileExists(fq1Out))
    check(fileExists(fq2Out))
    check(fileExists(bamOut))
    # BAM should contain twice as many reads as fastq in 
    check(bamReadCount(bamOut) == 2*fastqReadCount(fq1))
    # Both FastQ files should be empty  
    check(0 == fastqReadCount(fq1Out))
    check(0 == fastqReadCount(fq2Out))
    # clean up
    removeFile(fq1Out)
    removeFile(fq2Out)
    removeFile(bamOut)

  test "single end":
    # similar for SE
    decontanimate(fq1, "", reffa, outpref, reportEvery=0)
    check(fileExists(fq1Out))
    check(fileExists(bamOut))
    check(not fileExists(fq2Out))
    # BAM should contain twice as many reads as fastq in 
    check(bamReadCount(bamOut) == fastqReadCount(fq1))
    # Fastq out should be empty
    check(0 == fastqReadCount(fq1Out))
    # clean up
    removeFile(fq1Out)
    removeFile(bamOut)
      
  test "interleaved":
    decontanimate(fqi, "", reffa, outpref, reportEvery=0, isInterleaved=true)
    check(fileExists(fq1Out))
    check(fileExists(fq2Out))
    check(fileExists(bamOut))
    # BAM should contain as many reads as fastq in 
    check(bamReadCount(bamOut) == fastqReadCount(fqi))
    # Both FastQ files should be empty  
    check(0 == fastqReadCount(fq1Out))
    check(0 == fastqReadCount(fq2Out))
    # clean up
    removeFile(fq1Out)
    removeFile(fq2Out)
    removeFile(bamOut)